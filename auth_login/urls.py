from django.urls import path
from auth_login import views

app_name = 'auth'

urlpatterns = [
    path('login', views.login, name='login'), # auth/login
    path('logout', views.logout, name='logout') 
]

