from basket import views
from django.urls import path

app_name = 'basket'

urlpatterns = [
    path('players/list', views.players_list, name="players_list"),   
    path('teams/list', views.teams_list, name="teams_list"),   
    path('coachs/list', views.coachs_list, name="coachs_list"),   
    path('matchs/list', views.matchs_list, name="matchs_list"),

    path('create/team', views.create_team, name="create_team"),  
    path('create/player', views.create_player, name="create_player"),
    path('create/coach', views.create_coach, name="create_coach"),

    path('update/coach/<int:coach_id>', views.update_coach, name="update_coach"),
    path('update/team/<int:team_id>', views.update_team, name="update_team"), 
    path('update/player/<int:player_id>', views.update_player, name="update_player"),

    path('delete/coach/<int:coach_id>', views.delete_coach, name="delete_coach"),
    path('delete/team/<int:team_id>', views.delete_team, name="delete_team"),
    path('delete/player/<int:player_id>', views.delete_player, name="delete_player"),

    path('', views.home, name="home"),   
]

