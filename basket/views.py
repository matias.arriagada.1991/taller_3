from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib import messages
from django.core.paginator import (
Paginator, 
EmptyPage, 
PageNotAnInteger 
)
from basket.models import (
    Team,
    Player,
    Coach,
    Match
)
from basket.forms import CoachForm, PlayerForm, TeamForm

@login_required
def players_list(request):
    template_name = 'players_list.html'
    data = {}
    teams = Team.objects.all()

    for team in teams:
        team.players = Player.objects.filter(team=team)
        data['teams'] = teams


    #if 'q' in request.GET:
    #  query = request.GET.get('q')
    #  name = Player.objects.filter(name__icontains=query)
    #else:                
        #name = Player.objects.all()       

    return render(request, template_name, data)

@login_required
def teams_list(request):
    template_name = 'teams_list.html'
    data = {}

    #data['teams'] = Team.objects.all()
    page = request.GET.get('page', 1)
    team_lists = Team.objects.all()

    paginator = Paginator(team_lists, 2)

    try:
        data['teams'] = paginator.page(page)
    except PageNotAnInteger:
        data['teams'] = paginator.page(1)
    except EmptyPage:
        data['teams'] = paginator.page(paginator.num_pages)

    return render(request, template_name, data)


@login_required
def coachs_list(request):
    template_name = 'coach_list.html'
    data = {}
    data['coachs'] = Coach.objects.all()

    return render(request, template_name, data)

@login_required
def matchs_list(request):
    template_name = 'matchs_list.html'
    data = {}

    data['matchs'] = Match.objects.all()

    return render(request, template_name, data)

@login_required
def home(request):
    template_name = 'home.html'
    data = {}

    #if 'q' in request.GET:
    #    query = request.GET.get('q')
    # #   name = Player.objects.filter(name__icontains=query)
    #else:

    data['last_5_matchs'] = Match.objects.all().order_by('-pk')[:5]
    data['last_5_players'] = Player.objects.all().order_by('-pk')[:5]
    data['teams'] = Team.objects.all()

    return render(request, template_name, data)

@permission_required('basket.add_coach')
def create_coach(request):
    data = {}
    template_name = 'create_coach.html'

    data['form'] = CoachForm(request.POST or None)
    if data['form'].is_valid():
        data['form'].save()
        messages.add_message(
                    request, 
                    messages.SUCCESS, 
                    'Coach created.'
                )
        return  redirect('basket:coachs_list')

    return render(request, template_name, data)

@permission_required('basket.add_team')
def create_team(request):
    data = {}
    template_name = 'create_team.html'

    data['form'] = TeamForm(request.POST or None, request.FILES)
    if data['form'].is_valid():
        data['form'].save()
        return  redirect('basket:teams_list')

    return render(request, template_name, data)

@permission_required('basket.add_player')
def create_player(request):
    data = {}
    template_name = 'create_player.html'

    data['form'] = PlayerForm(request.POST or None, request.FILES)
    if data['form'].is_valid():
        data['form'].save()
        return  redirect('basket:players_list')

    return render(request, template_name, data)

@permission_required('basket.change_coach')
def update_coach(request, coach_id):
    template_name = 'update_coach.html'
    data = {}

    coach = Coach.objects.get(pk=coach_id)
    data['form'] = CoachForm(request.POST or None, instance=coach)

    if data['form'].is_valid():
        data['form'].save()
        return  redirect('basket:coachs_list')


    return render(request, template_name, data)

@permission_required('basket.change_team')
def update_team(request, team_id):
    template_name = 'update_team.html'
    data = {}

    team = Team.objects.get(pk=team_id)
    data['form'] = TeamForm(request.POST or None, instance=team)

    if data['form'].is_valid():
        data['form'].save()
        return  redirect('basket:teams_list')


    return render(request, template_name, data)

@permission_required('basket.change_player')
def update_player(request, player_id):
    template_name = 'update_player.html'
    data = {}

    #player = Player.objects.get(pk=player_id)
    #
    #if request.method == 'POST':
    #    data['form'] = PlayerForm(request.POST, files=request.FILES, instance=player)
    #    if data['form'].is_valid():
    #        player.save()
    #        return redirect('basket:players_list')
    #    else:
   #         data['form'] = PlayerForm(instance=player)

    
    player = Player.objects.get(pk=player_id)
    data['form'] = PlayerForm(request.POST or None, instance=player)

    if data['form'].is_valid():
        data['form'].save()
        return  redirect('basket:players_list')


    return render(request, template_name, data)

@permission_required('basket.delete_coach')
def delete_coach(request, coach_id):
    data = {}
    data['coach'] = Coach.objects.get(pk=coach_id)
    

    if request.method == 'POST':
        data['coach'].delete()
        return redirect('basket:coachs_list')

    template_name = 'delete_coach.html'
    return render(request, template_name, data)


@permission_required('basket.change_team')
def delete_team(request, team_id):
    data = {}
    data['team'] = Team.objects.get(pk=team_id)
    

    if request.method == 'POST':
        data['team'].delete()
        return redirect('basket:teams_list')

    template_name = 'delete_team.html'
    return render(request, template_name, data)

@permission_required('basket.change_player')
def delete_player(request, player_id):
    data = {}
    data['player'] = Player.objects.get(pk=player_id)
    

    if request.method == 'POST':
        data['player'].delete()
        return redirect('basket:players_list')

    template_name = 'delete_player.html'
    return render(request, template_name, data)